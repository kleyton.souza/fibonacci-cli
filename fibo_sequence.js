
class FiboSequence{
    constructor(sequence){
        this.sequence = sequence;
    }

    toShowSequence(){
        var fibo = []
        fibo[0] = 0
        fibo[1] = 1
        
        for (var i = 2; i < this.sequence; i++){ 
            fibo[i] = fibo[i-2]+fibo[i-1];
        }
        return fibo; 
    }

    crashIndex(){
        if (this.sequence > 100){
            return "Too many sequences, Do You think I'am a data center?! The limit is 100";
        }else{
            return false;
        }
    }

}
module.exports = FiboSequence;