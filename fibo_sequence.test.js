import FiboSequence from "./fibo_sequence";
import msgError from "./index.js";

describe('Fibo Sequence', () => {
  describe('Units', () => {
    test('should create a fibo sequence object', () => {
      const fiboSequence = new FiboSequence(1);
      expect(fiboSequence.toShowSequence()).toEqual([0, 1]);
    }); 

    test('should receive index 2 and show number 1', () => {
      const fiboSequence = new FiboSequence(2);
      expect(fiboSequence.toShowSequence().slice(-1)[0]).toEqual(1);
    });

    test('should receive index 3 and show number 1', () => {
      const fiboSequence = new FiboSequence(3);
      expect(fiboSequence.toShowSequence().slice(-1)[0]).toEqual(1);
    });

    test('should receive index 4 and show number 2', () => {
      const fiboSequence = new FiboSequence(4);
      expect(fiboSequence.toShowSequence().slice(-1)[0]).toEqual(2);
    });

    test('should receive index 5 and show number 3', () => {
      const fiboSequence = new FiboSequence(5);
      expect(fiboSequence.toShowSequence().slice(-1)[0]).toEqual(3);
    });

    test('should receive index 6 and show number 5', () => {
      const fiboSequence = new FiboSequence(6);
      expect(fiboSequence.toShowSequence().slice(-1)[0]).toEqual(5);
    });

    test('should receive index 7 and show number 8', () => {
      const fiboSequence = new FiboSequence(7);
      expect(fiboSequence.toShowSequence().slice(-1)[0]).toEqual(8);
    });

    test('should receive index 8 and show number 13', () => {
      const fiboSequence = new FiboSequence(8);
      expect(fiboSequence.toShowSequence().slice(-1)[0]).toEqual(13);
    });

    test('should receive index 9 and show number 21', () => {
      const fiboSequence = new FiboSequence(9);
      expect(fiboSequence.toShowSequence().slice(-1)[0]).toEqual(21);
    });

    test('should receive index 10 and show number 34', () => {
      const fiboSequence = new FiboSequence(10);
      expect(fiboSequence.toShowSequence().slice(-1)[0]).toEqual(34);
    });
  });

  describe('should accept a maximum index of 100', () => {
    test('should give a error msg when receive index 101', () => {
      const fiboSequence = new FiboSequence(101);
      console.log(fiboSequence.crashIndex())
      console.log(fiboSequence);
      expect(fiboSequence.crashIndex()).toEqual("Too many sequences, Do You think I'am a data center?! The limit is 100");
    });

    test('should receive index 100', () => {
      const fiboSequence = new FiboSequence(100);
      expect(fiboSequence.toShowSequence().slice(-1)[0]).toEqual(218922995834555200000);
    });
  });
});
