#!/usr/bin/env node

const yargs = require('yargs');
const FiboSequence = require('./fibo_sequence.js');

module.exports = () => {
	const options = yargs
	 .usage("Usage: -n <fibo sequence>")
	 .option("n", { alias: "fibo-sequence", describe: "Sequence fibo to show", type: "int", demandOption: true })
	 .argv;
  
	const fb = new FiboSequence(options.n);

	const msgCrashIndex = fb.crashIndex();
	
	if (msgCrashIndex){
		console.log(msgCrashIndex);
	}else{
		const show = fb.toShowSequence().slice(-1)[0]
		console.log("O "+options.n +"º elemento da sequencia de fibonacci é o numero : " +show);
	}
	
}


